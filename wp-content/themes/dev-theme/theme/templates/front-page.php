
<?php get_template_part('elements/intro'); ?>
<section id="latest-work">
  <div class="row">
    <h2>Laatste werk</h2>
    <?php get_template_part('elements/list-portfolio'); ?>
  </div>
</section>