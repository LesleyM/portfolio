<?php
/**
 * Template Name: static about page
 *
 * @package lesleyMerks
 */
?>

<?php get_header(); ?>

<?php get_template_part('elements/intro'); ?>
<div class="main-content">
  <?php get_template_part('elements/about-loop'); ?>
</div>
<?php get_footer(); ?>