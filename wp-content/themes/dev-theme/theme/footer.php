<?php
/**
 * Footer file common to all
 * templates
 *
 */
?>



<footer class="footer">
  <div class="footer__row">
    <span class="footer__copy"><?php echo date('Y'); ?> Lesley Merks</span>
    <ul class="footer__list">
      <li class="footer__item"><a class="footer__link" href="https://www.linkedin.com/in/lesleymerks
      " class="footer__link">Linkedin</a></li>
      <li class="footer__item"><a class="footer__link" href="https://dribbble.com/LesleyM" class="footer__link">Dribbble</a></li>
      <li class="footer__item"><a class="footer__link" href="mailto:info@lesleymerks.nl" class="footer__link">Mail</a></li>
    </ul>
  </div>
</footer>




<?php wp_footer(); ?>

<?php // </body> opens in header.php ?>
</div>
</body>
</html>