<?php
/**
 * Header file common to all
 * templates
 *
 */
?>
<!doctype html>
<html class="site" <?php language_attributes(); ?>>
<head>
	<!--[if lt IE 9]>
		<script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
	<![endif]-->

	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
	<meta name="viewport" content="width=device-width, initial-scale=1"/>

	<?php wp_head(); ?>
	<script async defer src="<?php echo get_template_directory_uri(); ?>/assets/js/core.js"></script>
</head>
<body class="<?php echo basename(get_permalink()); ?>">
<div class="wrapper">
  <header class="header animated fadeInUp">
    <div class="row">
      <h1 class="header__title"><a href="/" class="header__link">Lesley Merks</a></h1>
      <?php // <body> closes in footer.php

      $defaults = array(
        'echo'            => true,
        'fallback_cb'     => 'wp_page_menu',
        'depth'           => 0,
        'container'       => 'nav',
        'menu_id'         => '',
        'container_class' => '',
        'walker'          => new MOZ_Walker_Nav_Menu
      );

      wp_nav_menu( $defaults ); ?>
    </div>
  </header>

  <?php // common header content goes here ?>