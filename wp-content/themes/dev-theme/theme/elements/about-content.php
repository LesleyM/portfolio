<div class='about-block'>
  <h3 class="about-block__title"><?php the_sub_field('about_title'); ?></h3>
  <div class="about-block__content">
    <p>
      <?php the_sub_field('about_content'); ?>
    </p>
  </div>
</div>