<ul class="list-portfolio">
  <?php while( has_sub_field('portfolio_item') ): ?>
    <li class="list-portfolio__item">
      <a href="" class="list-portfolio__link">
        <img src="<?php the_sub_field('portfolio_image');?>" alt="" class="list-portfolio__image">
        <span class="list-portfolio__title"><?php the_sub_field('portfolio_title'); ?></span>
        <span class="list-portfolio__job"><?php the_sub_field('portfolio_job'); ?></span>
        <span class="list-portfolio__location"><?php the_sub_field('portfolio_location'); ?></span> 
        <span class="list-portfolio__date"><?php the_sub_field('portfolio_date'); ?></span> 
        <span class="list-portfolio__read-more">Meer</span>
      </a>
    </li>
  <?php endwhile; ?>
</ul>
