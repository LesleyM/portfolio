<?php $postid = get_the_ID(); ?>

<div class="intro <?php //TODO refactor this !! 
    if($postid == 7){ echo 'intro--large';} ?>">
  <div class="intro__row">
    <h1 class="intro__title <?php //TODO refactor this !! 
    if($postid == 7){ echo 'intro__title--large';} ?>"><?php echo get_field('intro_title', $postid); ?></h1>
    <?php if(!empty(get_field('intro_sub_title', $postid))) { ?>
      <span class="intro__text"><?php echo get_field('intro_sub_title', $postid); ?> 
        <?php if(!empty(get_field('intro_link', $postid))) { ?>
          <a class="intro__link" href="<?php echo get_field('intro_link', $postid); ?>"><?php echo get_field('link_name', $postid);?></a>
        <?php } ?>
      </span>
    <?php } ?>
  </div>
</div>