<div class="about-block">
  <h3 class="about-block__title"><?php the_sub_field('resume_title') ?></h3>
  <div class="about-block__content">
    <ul class="list-resume">
      <?php while( has_sub_field('resume_item') ): ?>
        <li class="list-resume__item">
        <?php if (get_sub_field('resume_year')): ?>
          <span class="list-resume__year"><?php the_sub_field('resume_year'); ?></span>
        <?php endif; ?>
          <span class="list-resume__title"><?php the_sub_field('resume_title'); ?></span> 
          <span class="list-resume__job-title"><?php the_sub_field('resume_job'); ?></span>
          <span class="list-resume__timespan <?php if(get_sub_field('resume_current_')){ echo 'list-resume__timespan--current';} ?>"><?php the_sub_field('resume_timespan'); ?></span>
          
        </li>
      <?php endwhile; ?>
    </ul>
  </div>
</div>