<div class="about-block">
  <h3 class="about-block__title"><?php the_sub_field('list_title') ?></h3>
  <div class="about-block__content">
    <ul class="list-plain">
      <?php while( has_sub_field('list_plain') ): ?>
        <li class="list-plain__item<?php if(get_sub_field('grey')){ echo " list-plain__item--grey"; } ?>">
          <span class="list-plain__title"><?php the_sub_field('item_title'); ?></span>
          <?php if (get_sub_field('item_content')): ?>
            <span class="list-plain__content"><?php the_sub_field('item_content'); ?></span> 
          <?php endif; ?>
        </li>
      <?php endwhile; ?>
    </ul>
  </div>
</div>