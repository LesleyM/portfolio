<?php $post_id = get_the_ID(); ;?>
  <?php while( has_sub_field("about_page") ): ?>
      <?php 
        if (get_row_layout() == 'list-plain' ) :
          get_template_part('elements/list-plain');
        elseif(get_row_layout() == 'list_testemonial') :
          get_template_part('elements/list-testemonial');
        elseif(get_row_layout() == 'list_resume') :
          get_template_part('elements/list-resume');
        elseif(get_row_layout() == 'about_content') :
          get_template_part('elements/about-content');
      
        endif;
      ?>
      
  <?php endwhile; ?>
